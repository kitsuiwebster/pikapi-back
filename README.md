# Pikapi - Back-end - WIP

## Prod backend

- `ssh kitsui@<vps-ip>`
- `screen -r back`
- `npm run start` if not running.

## Local backend

- `npm run start`
- if needed, comment/uncomment in the main.ts

## Local frontend

- `yarn start`
