// import * as dotenv from 'dotenv';
// dotenv.config();

// import { NestFactory } from '@nestjs/core';
// import { AppModule } from './app.module';
// import { readFileSync } from 'fs';
// import { HttpsOptions } from '@nestjs/common/interfaces/external/https-options.interface';
// import * as express from 'express';
// import { ExpressAdapter } from '@nestjs/platform-express';

// async function bootstrap() {
//   const httpsOptions: HttpsOptions = {
//     key: readFileSync('/home/kitsui/ssl_certs/privkey.pem'),
//     cert: readFileSync('/home/kitsui/ssl_certs/fullchain.pem'),
//   };

//   const server = express();
//   const app = await NestFactory.create(
//     AppModule,
//     new ExpressAdapter(server),
//     { httpsOptions }
//   );

//   app.enableCors();

//   await app.listen(4001, () => console.log('Server running on https://api.pikapi.co:4001'));
// }

// bootstrap();

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  await app.listen(4001, () => console.log('Server running on http://localhost:4001'));
}
bootstrap();
