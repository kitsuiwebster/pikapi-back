import * as Nano from 'nano';
import * as dotenv from 'dotenv';

dotenv.config();

declare function emit(key: any, value: any): void;

const db = Nano(process.env.COUCHDB_URL).use('pikapi');

interface DesignDocument {
  _id: string;
  views: {
    designDoc: {
      map: (doc: any) => void;
    };
  };
  _rev?: string; // Add the optional _rev property
}

const designDoc = {
  _id: '_design/user_queries',
  views: {
    byUsernameOrEmail: {
      map: function(doc) {
        if (doc.type === 'users') {
          emit(["users", "username", doc.username], doc.username);
          emit(["users", "email", doc.email], doc.email);
        }
      }.toString()
    }
  }
};


// db.get('_design/user_queries', function(err, existing) {
//   if (!err && existing) {
//     designDoc._rev = existing._rev; // Set the latest _rev from the existing document
//   }
//   db.insert(designDoc, function(err, body) {
//     if (err) {
//       console.error(err);
//     } else {
//       console.log('View created or updated successfully');
//     }
//   });
// });

