// app.module.ts
import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { AuthModule } from './modules/auth.module'; // Adjust the path as necessary
import { CouchDbService } from './services/couchdb.service';
import { PictureModule } from './modules/picture.module';
import { PaymentsModule } from './modules/payments.module';

@Module({
  imports: [
    PaymentsModule,
    AuthModule,
    PictureModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
  ],
  providers: [CouchDbService],
})
export class AppModule {}
