import { Module } from '@nestjs/common';
import { PictureController } from '../controllers/picture.controller';
import { CouchDbService } from '../services/couchdb.service';

@Module({
  controllers: [PictureController],
  providers: [CouchDbService],
})
export class PictureModule {}
