// auth.module.ts
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from '../guards/jwt.strategy';
import { UserService } from '../services/user.service';
import { UserController } from '../controllers/user.controller';
import { CouchDbService } from '../services/couchdb.service'; // import CouchDbService

@Module({
  imports: [
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: {},
    }),
  ],
  controllers: [UserController],
  providers: [UserService, JwtStrategy, CouchDbService], // add CouchDbService to the providers array
})
export class AuthModule {}
