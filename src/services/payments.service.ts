import { Injectable } from '@nestjs/common';
import Stripe from 'stripe';

@Injectable()
export class PaymentsService {
    private stripe: Stripe;

    constructor() {
        this.stripe = new Stripe(process.env.STRIPE_TESTING_SECRET_KEY, {
        apiVersion: '2024-04-10',
        });
    }

    async createCheckoutSession(productDetails: { name: string; amount: number }, pictureId: string): Promise<Stripe.Checkout.Session> {
        const { name, amount } = productDetails;
        console.log('---> Received product details for checkout:', productDetails);

        return this.stripe.checkout.sessions.create({
        payment_method_types: ['card', 'paypal'],
        line_items: [{
            price_data: {
            currency: 'usd',
            product_data: {
                name,
            },
            unit_amount: amount,
            },
            quantity: 1,
        }],
        mode: 'payment',
        success_url: `${process.env.FRONTEND_URL}/success?session_id={CHECKOUT_SESSION_ID}`,
        cancel_url: `${process.env.FRONTEND_URL}/cancel`,
        metadata: {
            pictureId: pictureId
        }
        });
    }


    async createCartCheckoutSession(items: { name: string; amount: number; quantity: number; downloadId: string }[]): Promise<Stripe.Checkout.Session> {
        const line_items = items.map(item => ({
            price_data: {
                currency: 'usd',
                product_data: {
                    name: item.name,
                },
                unit_amount: item.amount,
            },
            quantity: item.quantity,
        }));
    
        // Construction des métadonnées pour inclure les identifiants de téléchargement
        let metadata = {};
        items.forEach((item, index) => {
            metadata[`item_${index}_downloadId`] = item.downloadId;
        });
    
        return this.stripe.checkout.sessions.create({
            payment_method_types: ['card', 'paypal'],
            line_items,
            mode: 'payment',
            success_url: `${process.env.FRONTEND_URL}/shop-payment-success?session_id={CHECKOUT_SESSION_ID}`,
            cancel_url: `${process.env.FRONTEND_URL}/cancel`,
            metadata: metadata
        });
    }
      

    async verifyPayment(sessionId: string): Promise<{ success: boolean, pictureId?: string }> {
        const session = await this.stripe.checkout.sessions.retrieve(sessionId);
        const paymentIntent = await this.stripe.paymentIntents.retrieve(session.payment_intent as string);
        const pictureId = session.metadata.pictureId;  
        return { success: paymentIntent.status === 'succeeded', pictureId };
    }    
}
