import { Injectable } from '@nestjs/common';
import * as Nano from 'nano';
import * as dotenv from 'dotenv';

dotenv.config();


@Injectable()
export class CouchDbService {
  private readonly nano: Nano.DocumentScope<any>;

  constructor() {
    const db = Nano(process.env.COUCHDB_URL);

    this.nano = db.use('pikapi');
  }


  async createDoc(docType: string, doc: any): Promise<any> {
    try {
      const response = await this.nano.insert({
        _id: `${docType}:${doc._id}`,
        type: docType,
        ...doc,
      });
      console.log('\n---> Document created in CouchDB:', response); // Log the CouchDB response
      return response;
    } catch (error) {
      console.error('\n!!--> Error creating CouchDB document:', error); // Log the CouchDB error
      throw error; // Re-throw to be handled by the caller
    }
  }


  async findDoc(docType: string, field: string, value: string): Promise<any> {
    if (field === '_id') {
      // Directly use the value, assuming it already includes the docType prefix
      return this.findDocById(value);
    } else {
      return this.findDocByField(docType, field, value);
    }
  }

  
  async findDocById(docId: string): Promise<any> {
    try {
      const doc = await this.nano.get(docId);
      return doc;
    } catch (error) {
      console.error('\n!!--> Error finding document by _id in CouchDB:', error);
      throw error;
    }
  }

  // Method to query documents based on fields other than _id, using the CouchDB view
  async findDocByField(docType: string, field: string, value: string): Promise<any> {
    try {
      // Ensure the field is neither '_id' nor a field not covered by the view
      if (field !== '_id' && (field === 'username' || field === 'email')) {
        const response = await this.nano.view('user_queries', 'byUsernameOrEmail', {
          key: [docType, field, value],
          include_docs: true
        });
        if (response.rows.length === 0) {
          return undefined;
        }
        return response.rows[0].doc; // Use .doc to get the actual document
      } else {
        console.error('\n!!--> Unsupported field for querying:', field);
        throw new Error('Unsupported field for querying');
      }
    } catch (error) {
      console.error('\n!!--> Error querying document by field in CouchDB:', error);
      throw error;
    }
  }

  
  
  
  

  async findAll(): Promise<any[]> {
    try {
      const result = await this.nano.list({ include_docs: true });
      return result.rows.map((row) => row.doc);
    } catch (error) {
      // Handle or throw the error accordingly
      throw new Error('Unable to fetch documents');
    }
  }

  async updateDoc(updatedDoc: any): Promise<any> {
    try {
        // Directly use the _id provided by updatedDoc, assuming it's correctly formatted
        const response = await this.nano.insert({
            ...updatedDoc,
            _rev: updatedDoc._rev // Ensure the _rev is passed for update
        });

        console.log('\n---> Document updated in CouchDB:', response);
        return response;
    } catch (error) {
        console.error('\n!!--> Error updating CouchDB document:', error);
        throw error;
    }
  }


  async findAllPictures(): Promise<any[]> {
    try {
      const result = await this.nano.view('picture_queries', 'all_pictures', { include_docs: true });
      return result.rows.map(row => row.doc);
    } catch (error) {
      console.error('\n!!--> Error fetching all pictures:', error);
      throw error;
    }
  }

  async findPictureById(pictureId: string): Promise<any> {
    try {
      const doc = await this.nano.get(pictureId);
      return doc;
    } catch (error) {
      console.error(`\n!!--> Error fetching picture with ID ${pictureId}:`, error);
      throw new Error();
    }
  }
}
