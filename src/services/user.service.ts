import { Injectable } from '@nestjs/common';
import { User } from '../models/user.model';
import * as bcrypt from 'bcryptjs';
import { CouchDbService } from '../services/couchdb.service';
import { v4 as uuidv4 } from 'uuid';


@Injectable()
export class UserService {
  constructor(private readonly couchDbService: CouchDbService) {}

  async createUser(username: string, password: string, email: string): Promise<User> {
    const hashedPassword = await bcrypt.hash(password, 10);
    const _id = uuidv4(); // Generate a new UUID for the user
    const favorites: string[] = []; // Initialize favorites as an empty array
    const newUser = new User(_id, username, hashedPassword, email, favorites);
    
    // Save the newUser to the database...
    await this.couchDbService.createDoc('users', newUser);
    console.log('\n---> User successfully created:', newUser); // Log the successful creation
    return newUser;
  }
  
  async findUserByUsernameOrEmail(usernameOrEmail: string): Promise<User | undefined> {
    const isEmail = usernameOrEmail.includes('@');
    let userDoc;
  
    if (isEmail) {
      userDoc = await this.couchDbService.findDoc('users', 'email', usernameOrEmail);
    } else {
      userDoc = await this.couchDbService.findDoc('users', 'username', usernameOrEmail);
    }
  
    if (!userDoc) return undefined;

    // Correctly instantiate the User with the expected constructor arguments
    return new User(userDoc._id, userDoc.username, userDoc.password, userDoc.email, userDoc.favorites || []);
  }
  
  // Assuming 'userId' is correctly formatted as per your database expectations
  async addFavorite(userId: string, pictureId: string): Promise<boolean> {
    try {
        // Fetch the user document based on its _id
        const userDoc = await this.couchDbService.findDoc('users', '_id', userId);
        if (!userDoc || !userDoc.favorites) {
            console.error('\n!!--> User not found or no favorites array:', userId);
            return false;
        }

        if (userDoc.favorites.includes(pictureId)) {
            console.log('\n---> Picture already in favorites');
            return false;
        }

        userDoc.favorites.push(pictureId);

        // Update the user document with the modified favorites array
        await this.couchDbService.updateDoc(userDoc); // Ensure the document is correctly identified for update
        return true;
    } catch (error) {
        console.error('\n!!--> Error adding favorite:', error);
        return false;
    }
  }

  async removeFavorite(userId: string, pictureId: string): Promise<boolean> {
    try {
        const userDoc = await this.couchDbService.findDoc('users', '_id', userId);
        if (!userDoc || !userDoc.favorites) {
            console.error('\n!!--> User not found or no favorites array:', userId);
            return false;
        }

        const index = userDoc.favorites.indexOf(pictureId);
        if (index === -1) {
            console.log('\n---> Picture not in favorites');
            return false;
        }

        userDoc.favorites.splice(index, 1);

        // Update the user document with the modified favorites array
        await this.couchDbService.updateDoc(userDoc); // Ensure the document is correctly identified for update
        return true;
    } catch (error) {
        console.error('\n!!--> Error removing favorite:', error);
        return false;
    }
  }



  async findOne(userId: string): Promise<User | undefined> {
    console.log(`\n---> UserService: Finding user with ID ${userId}`);
    // Directly use the userId without prefixing
    const userDoc = await this.couchDbService.findDoc('users', '_id', userId);
    console.log(`\n---> UserService: User document: ${JSON.stringify(userDoc)}`);
    if (!userDoc) return undefined;
  
    return new User(userDoc._id, userDoc.username, userDoc.password, userDoc.email, userDoc.favorites || []);
  }

}
