// user.controller.ts
import { Response } from 'express';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import * as bcrypt from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';
import { Body, Controller, Post, Get, Param, Res, UseGuards, Req } from '@nestjs/common';
import { CustomRequest } from '../interfaces/custom-request.interface';


@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  @Post('register')
  async register(@Body() body: User, @Res() res: Response) {
    try {
      const newUser = await this.userService.createUser(
        body.username,
        body.password,
        body.email,
      );
      // Include a success property in the response
      res.status(201).send({ success: true, user: newUser });
    } catch (error) {
      console.error('Error during registration:', error);
      // You can also send the error details to the client if needed
      res.status(500).send({ success: false, message: 'Internal Server Error' });
    }
  }

  

  @Post('login')
  async login(@Body() body: any, @Res() res: Response) {
    console.log("Login request received."); 
    try {
      const user = await this.userService.findUserByUsernameOrEmail(body.usernameOrEmail);
      if (!user) {
        console.log(`User not found for identifier: ${body.usernameOrEmail}`);
        return res.status(401).send({ success: false, message: 'Invalid username or password' });
      }
      const passwordsMatch = await bcrypt.compare(body.password, user.password);
      if (!passwordsMatch) {
        console.log('Password does not match');
        return res.status(401).send({ success: false, message: 'Invalid username or password' });
      }
      const payload = { username: user.username, sub: user._id };
      const jwtToken = this.jwtService.sign(payload);
      console.log('Login successful, token:', jwtToken);
      // Include user ID in the login response
      return res.status(200).send({ success: true, jwtToken, userId: user._id });
    } catch (error) {
      console.error('Login error:', error);
      return res.status(500).send({ success: false, message: 'Internal Server Error' });
    }
  }


  @UseGuards(JwtAuthGuard) // Apply the JwtAuthGuard to protect the route
  @Post('favorites/add')
  async addFavorite(@Req() req: CustomRequest, @Body('pictureId') pictureId: string, @Res() res: Response) {
    // Extract the user ID from the request after successful authentication
    const userId = req.user['_id']; // Make sure the user object has an _id property
    const success = await this.userService.addFavorite(userId, pictureId);
    if (success) {
      res.status(200).send({ success: true, message: 'Favorite added successfully' });
    } else {
      res.status(400).send({ success: false, message: 'Could not add favorite' });
    }
  }

  @UseGuards(JwtAuthGuard) // Apply the JwtAuthGuard to protect the route
  @Post('favorites/remove')
  async removeFavorite(@Req() req: CustomRequest, @Body('pictureId') pictureId: string, @Res() res: Response) {
    // Extract the user ID from the request after successful authentication
    const userId = req.user['_id']; // Make sure the user object has an _id property
    const success = await this.userService.removeFavorite(userId, pictureId);
    if (success) {
      res.status(200).send({ success: true, message: 'Favorite removed successfully' });
    } else {
      res.status(400).send({ success: false, message: 'Could not remove favorite' });
    }
  }


  @Get(':userId')
  async getUserById(@Param('userId') userId: string, @Res() res: Response) {
    try {
      const userDoc = await this.userService.findOne(userId);
      if (!userDoc) {
        return res.status(404).send({ success: false, message: 'User not found' });
      }
      res.status(200).send({ success: true, user: userDoc });
    } catch (error) {
      console.error('Error retrieving user by ID:', error);
      res.status(500).send({ success: false, message: 'Internal Server Error' });
    }
  }
}
