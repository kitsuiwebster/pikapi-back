import { Controller, Get, Param, Res, UseGuards, NotFoundException } from '@nestjs/common';
import { CouchDbService } from '../services/couchdb.service';
import { AuthGuard } from '@nestjs/passport'; // or any auth guard you use for securing endpoints
import axios from 'axios';
import { Response } from 'express';


@Controller('pictures')
export class PictureController {
  constructor(private readonly couchDbService: CouchDbService) {}

  @Get()
  async getAllPictures(@Res() res: Response) {
    try {
      const pictures = await this.couchDbService.findAllPictures();
      res.status(200).send(pictures);
    } catch (error) {
      console.error('Error getting all pictures:', error);
      res.status(500).send({ success: false, message: 'Internal Server Error' });
    }
  }

  @Get(':id')
  async getPictureById(@Param('id') id: string, @Res() res: Response) {
    try {
      const picture = await this.couchDbService.findPictureById(id);
      if (!picture) {
        return res.status(404).send({ success: false, message: 'Picture not found' });
      }
      res.status(200).send(picture);
    } catch (error) {
      console.error(`Error getting picture with ID ${id}:`, error);
      res.status(500).send({ success: false, message: 'Internal Server Error' });
    }
  }

  @Get('/secure-download/:id')
  async secureDownload(@Param('id') id: string, @Res() res: Response) {
    try {
      const picture = await this.couchDbService.findPictureById(id);
      if (!picture) {
        throw new NotFoundException('Picture not found');
      }

      const response = await axios.get(picture.link4k, {
        responseType: 'stream', // This ensures that you get the data as a stream
      });

      // Forward the headers from the Google Drive download link
      res.setHeader('Content-Disposition', response.headers['content-disposition']);

      // Pipe the download stream directly to the client
      response.data.pipe(res);
    } catch (error) {
      console.error(`Error during secure file download for picture with ID ${id}:`, error);
    
      // Check if this is an Axios error
      if (axios.isAxiosError(error)) {
        if (error.response) {
          // Handle error response from Axios request
          res.status(error.response.status).json({
            success: false,
            message: error.response.data?.message || 'An error occurred'
          });
        } else {
          // Handle case where there's no response (network error, etc.)
          res.status(500).json({
            success: false,
            message: 'Network Error or no response received'
          });
        }
      } else if (error instanceof NotFoundException) {
        // Handle custom NotFoundException
        res.status(404).json({ success: false, message: error.message });
      } else {
        // Handle any other error
        res.status(500).json({ success: false, message: 'Internal Server Error' });
      }
    }
    
  }
}
