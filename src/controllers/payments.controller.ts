import { Body, Controller, Post, HttpCode, HttpStatus, Get, Query } from '@nestjs/common';
import { PaymentsService } from '../services/payments.service';

@Controller('payments')
export class PaymentsController {
  constructor(private readonly paymentsService: PaymentsService) {}

  @Post('create-checkout-session')
  @HttpCode(HttpStatus.CREATED)
  async createCheckoutSession(@Body() body: { productDetails: { name: string; amount: number }, pictureId: string }) {
    const { productDetails, pictureId } = body;
    const session = await this.paymentsService.createCheckoutSession(productDetails, pictureId);
    return { sessionId: session.id };
  }

  // @Post('create-cart-checkout-session')
  // @HttpCode(HttpStatus.CREATED)
  // async createCartCheckoutSession(@Body() body: { items: { name: string; amount: number; quantity: number }[] }) {
  //     const session = await this.paymentsService.createCartCheckoutSession(body.items);
  //     return { sessionId: session.id };
  // }


  @Get('verify-payment')
  async verifyPayment(@Query('session_id') sessionId: string): Promise<{ success: boolean }> {
    return this.paymentsService.verifyPayment(sessionId);
  }
}
