import * as bcrypt from 'bcryptjs';

export class User {
    _id: string;
    username: string;
    password: string;
    email: string;
    favorites: string[]; // This will store the array of picture IDs marked as favorites

    constructor(_id: string, username: string, password: string, email: string, favorites: string[] = []) {
        this._id = _id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.favorites = favorites; // Initialize with an empty array if no favorites are provided
    }

    async passwordIsValid(plaintextPassword: string): Promise<boolean> {
        return await bcrypt.compare(plaintextPassword, this.password);
    }

    // You might have other methods here for the User model as needed
}
