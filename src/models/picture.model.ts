export class Picture {
    _id: string;
    src: string;
    alt: string;
    link1k: string;
    link4k: string;
    tags: string[];
  
    constructor(_id: string, src: string, alt: string, link1k:string, link4k: string, tags: string[]) {
      this._id = _id;
      this.src = src;
      this.alt = alt;
      this.link1k = link1k;
      this.link4k = link4k;
      this.tags = tags;
    }
}
  