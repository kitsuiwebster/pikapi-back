// jwt.strategy.ts
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { UserService } from '../services/user.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(payload: any) {
    console.log(`JwtStrategy: Validating user with ID ${payload.sub}`);
    try {
      const user = await this.userService.findOne(payload.sub);
      if (!user) {
        console.error('JwtStrategy: User not found');
        throw new Error('User not found');
      }
      return user;
    } catch (error) {
      console.error(`JwtStrategy: Error validating user: ${error.message}`);
      throw error;
    }
  }
}
